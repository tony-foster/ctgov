cube(`OverallOfficials`, {
  sql: `SELECT * FROM ctgov.overall_officials`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    role: {
      sql: `role`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    affiliation: {
      sql: `affiliation`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
