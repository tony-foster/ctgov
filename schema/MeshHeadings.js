cube(`MeshHeadings`, {
  sql: `SELECT * FROM ctgov.mesh_headings`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id]
    }
  },
  
  dimensions: {
    qualifier: {
      sql: `qualifier`,
      type: `string`
    },
    
    heading: {
      sql: `heading`,
      type: `string`
    },
    
    subcategory: {
      sql: `subcategory`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
