cube(`Categories`, {
  sql: `SELECT * FROM ctgov.categories`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id, createdAt, updatedAt]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    grouping: {
      sql: `grouping`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    createdAt: {
      sql: `created_at`,
      type: `time`
    },
    
    updatedAt: {
      sql: `updated_at`,
      type: `time`
    },
    
    lastModified: {
      sql: `last_modified`,
      type: `time`
    }
  }
});
