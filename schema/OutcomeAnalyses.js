cube(`OutcomeAnalyses`, {
  sql: `SELECT * FROM ctgov.outcome_analyses`,
  
  joins: {
    Outcomes: {
      sql: `${CUBE}.outcome_id = ${Outcomes}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, ciNSides, id]
    },
    
    paramValue: {
      sql: `param_value`,
      type: `sum`
    },
    
    dispersionValue: {
      sql: `dispersion_value`,
      type: `sum`
    },
    
    pValue: {
      sql: `p_value`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    nonInferiorityType: {
      sql: `non_inferiority_type`,
      type: `string`
    },
    
    nonInferiorityDescription: {
      sql: `non_inferiority_description`,
      type: `string`
    },
    
    paramType: {
      sql: `param_type`,
      type: `string`
    },
    
    dispersionType: {
      sql: `dispersion_type`,
      type: `string`
    },
    
    pValueModifier: {
      sql: `p_value_modifier`,
      type: `string`
    },
    
    ciNSides: {
      sql: `ci_n_sides`,
      type: `string`
    },
    
    ciUpperLimitNaComment: {
      sql: `ci_upper_limit_na_comment`,
      type: `string`
    },
    
    pValueDescription: {
      sql: `p_value_description`,
      type: `string`
    },
    
    method: {
      sql: `method`,
      type: `string`
    },
    
    methodDescription: {
      sql: `method_description`,
      type: `string`
    },
    
    estimateDescription: {
      sql: `estimate_description`,
      type: `string`
    },
    
    groupsDescription: {
      sql: `groups_description`,
      type: `string`
    },
    
    otherAnalysisDescription: {
      sql: `other_analysis_description`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
