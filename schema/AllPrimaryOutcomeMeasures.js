cube(`AllPrimaryOutcomeMeasures`, {
  sql: `SELECT * FROM ctgov.all_primary_outcome_measures`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, names]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    names: {
      sql: `names`,
      type: `string`
    }
  }
});
