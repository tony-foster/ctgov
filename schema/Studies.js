cube(`Studies`, {
  sql: `SELECT * FROM ctgov.studies`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, nlmDownloadDateDescription, studyFirstPostedDateType, resultsFirstPostedDateType, dispositionFirstPostedDateType, lastUpdatePostedDateType, startDateType, completionDateType, primaryCompletionDateType, briefTitle, officialTitle, expandedAccessTypeIndividual, createdAt, lastUpdateSubmittedDate, lastUpdateSubmittedQcDate, lastUpdatePostedDate, updatedAt, studyFirstSubmittedDate, resultsFirstSubmittedDate, dispositionFirstSubmittedDate, studyFirstSubmittedQcDate, studyFirstPostedDate, resultsFirstSubmittedQcDate, resultsFirstPostedDate, dispositionFirstSubmittedQcDate, dispositionFirstPostedDate, startDate, verificationDate, completionDate, primaryCompletionDate]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    nlmDownloadDateDescription: {
      sql: `nlm_download_date_description`,
      type: `string`
    },
    
    studyFirstPostedDateType: {
      sql: `study_first_posted_date_type`,
      type: `string`
    },
    
    resultsFirstPostedDateType: {
      sql: `results_first_posted_date_type`,
      type: `string`
    },
    
    dispositionFirstPostedDateType: {
      sql: `disposition_first_posted_date_type`,
      type: `string`
    },
    
    lastUpdatePostedDateType: {
      sql: `last_update_posted_date_type`,
      type: `string`
    },
    
    startMonthYear: {
      sql: `start_month_year`,
      type: `string`
    },
    
    startDateType: {
      sql: `start_date_type`,
      type: `string`
    },
    
    verificationMonthYear: {
      sql: `verification_month_year`,
      type: `string`
    },
    
    completionMonthYear: {
      sql: `completion_month_year`,
      type: `string`
    },
    
    completionDateType: {
      sql: `completion_date_type`,
      type: `string`
    },
    
    primaryCompletionMonthYear: {
      sql: `primary_completion_month_year`,
      type: `string`
    },
    
    primaryCompletionDateType: {
      sql: `primary_completion_date_type`,
      type: `string`
    },
    
    targetDuration: {
      sql: `target_duration`,
      type: `string`
    },
    
    studyType: {
      sql: `study_type`,
      type: `string`
    },
    
    acronym: {
      sql: `acronym`,
      type: `string`
    },
    
    baselinePopulation: {
      sql: `baseline_population`,
      type: `string`
    },
    
    briefTitle: {
      sql: `brief_title`,
      type: `string`
    },
    
    officialTitle: {
      sql: `official_title`,
      type: `string`
    },
    
    overallStatus: {
      sql: `overall_status`,
      type: `string`
    },
    
    lastKnownStatus: {
      sql: `last_known_status`,
      type: `string`
    },
    
    phase: {
      sql: `phase`,
      type: `string`
    },
    
    enrollmentType: {
      sql: `enrollment_type`,
      type: `string`
    },
    
    source: {
      sql: `source`,
      type: `string`
    },
    
    limitationsAndCaveats: {
      sql: `limitations_and_caveats`,
      type: `string`
    },
    
    whyStopped: {
      sql: `why_stopped`,
      type: `string`
    },
    
    hasExpandedAccess: {
      sql: `has_expanded_access`,
      type: `string`
    },
    
    expandedAccessTypeIndividual: {
      sql: `expanded_access_type_individual`,
      type: `string`
    },
    
    expandedAccessTypeIntermediate: {
      sql: `expanded_access_type_intermediate`,
      type: `string`
    },
    
    expandedAccessTypeTreatment: {
      sql: `expanded_access_type_treatment`,
      type: `string`
    },
    
    hasDmc: {
      sql: `has_dmc`,
      type: `string`
    },
    
    isFdaRegulatedDrug: {
      sql: `is_fda_regulated_drug`,
      type: `string`
    },
    
    isFdaRegulatedDevice: {
      sql: `is_fda_regulated_device`,
      type: `string`
    },
    
    isUnapprovedDevice: {
      sql: `is_unapproved_device`,
      type: `string`
    },
    
    isPpsd: {
      sql: `is_ppsd`,
      type: `string`
    },
    
    isUsExport: {
      sql: `is_us_export`,
      type: `string`
    },
    
    biospecRetention: {
      sql: `biospec_retention`,
      type: `string`
    },
    
    biospecDescription: {
      sql: `biospec_description`,
      type: `string`
    },
    
    ipdTimeFrame: {
      sql: `ipd_time_frame`,
      type: `string`
    },
    
    ipdAccessCriteria: {
      sql: `ipd_access_criteria`,
      type: `string`
    },
    
    ipdUrl: {
      sql: `ipd_url`,
      type: `string`
    },
    
    planToShareIpd: {
      sql: `plan_to_share_ipd`,
      type: `string`
    },
    
    planToShareIpdDescription: {
      sql: `plan_to_share_ipd_description`,
      type: `string`
    },
    
    createdAt: {
      sql: `created_at`,
      type: `time`
    },
    
    lastUpdateSubmittedDate: {
      sql: `last_update_submitted_date`,
      type: `time`
    },
    
    lastUpdateSubmittedQcDate: {
      sql: `last_update_submitted_qc_date`,
      type: `time`
    },
    
    lastUpdatePostedDate: {
      sql: `last_update_posted_date`,
      type: `time`
    },
    
    updatedAt: {
      sql: `updated_at`,
      type: `time`
    },
    
    studyFirstSubmittedDate: {
      sql: `study_first_submitted_date`,
      type: `time`
    },
    
    resultsFirstSubmittedDate: {
      sql: `results_first_submitted_date`,
      type: `time`
    },
    
    dispositionFirstSubmittedDate: {
      sql: `disposition_first_submitted_date`,
      type: `time`
    },
    
    studyFirstSubmittedQcDate: {
      sql: `study_first_submitted_qc_date`,
      type: `time`
    },
    
    studyFirstPostedDate: {
      sql: `study_first_posted_date`,
      type: `time`
    },
    
    resultsFirstSubmittedQcDate: {
      sql: `results_first_submitted_qc_date`,
      type: `time`
    },
    
    resultsFirstPostedDate: {
      sql: `results_first_posted_date`,
      type: `time`
    },
    
    dispositionFirstSubmittedQcDate: {
      sql: `disposition_first_submitted_qc_date`,
      type: `time`
    },
    
    dispositionFirstPostedDate: {
      sql: `disposition_first_posted_date`,
      type: `time`
    },
    
    startDate: {
      sql: `start_date`,
      type: `time`
    },
    
    verificationDate: {
      sql: `verification_date`,
      type: `time`
    },
    
    completionDate: {
      sql: `completion_date`,
      type: `time`
    },
    
    primaryCompletionDate: {
      sql: `primary_completion_date`,
      type: `time`
    }
  }
});
