cube(`DropWithdrawals`, {
  sql: `SELECT * FROM ctgov.drop_withdrawals`,
  
  joins: {
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      sql: `count`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    period: {
      sql: `period`,
      type: `string`
    },
    
    reason: {
      sql: `reason`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
