cube(`CalculatedValues`, {
  sql: `SELECT * FROM ctgov.calculated_values`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id, nlmDownloadDate]
    },
    
    actualDuration: {
      sql: `actual_duration`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    hasUsFacility: {
      sql: `has_us_facility`,
      type: `string`
    },
    
    minimumAgeUnit: {
      sql: `minimum_age_unit`,
      type: `string`
    },
    
    maximumAgeUnit: {
      sql: `maximum_age_unit`,
      type: `string`
    },
    
    wereResultsReported: {
      sql: `were_results_reported`,
      type: `string`
    },
    
    hasSingleFacility: {
      sql: `has_single_facility`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    nlmDownloadDate: {
      sql: `nlm_download_date`,
      type: `time`
    }
  }
});
