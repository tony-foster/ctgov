cube(`ResultGroups`, {
  sql: `SELECT * FROM ctgov.result_groups`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, title, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    resultType: {
      sql: `result_type`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
