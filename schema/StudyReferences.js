cube(`StudyReferences`, {
  sql: `SELECT * FROM ctgov.study_references`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, pmid, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    pmid: {
      sql: `pmid`,
      type: `string`
    },
    
    referenceType: {
      sql: `reference_type`,
      type: `string`
    },
    
    citation: {
      sql: `citation`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
