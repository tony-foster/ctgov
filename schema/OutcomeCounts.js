cube(`OutcomeCounts`, {
  sql: `SELECT * FROM ctgov.outcome_counts`,
  
  joins: {
    Outcomes: {
      sql: `${CUBE}.outcome_id = ${Outcomes}.id`,
      relationship: `belongsTo`
    },
    
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      sql: `count`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    scope: {
      sql: `scope`,
      type: `string`
    },
    
    units: {
      sql: `units`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
