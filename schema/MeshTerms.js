cube(`MeshTerms`, {
  sql: `SELECT * FROM ctgov.mesh_terms`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [id]
    }
  },
  
  dimensions: {
    qualifier: {
      sql: `qualifier`,
      type: `string`
    },
    
    treeNumber: {
      sql: `tree_number`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    meshTerm: {
      sql: `mesh_term`,
      type: `string`
    },
    
    downcaseMeshTerm: {
      sql: `downcase_mesh_term`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
