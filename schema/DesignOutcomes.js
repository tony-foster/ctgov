cube(`DesignOutcomes`, {
  sql: `SELECT * FROM ctgov.design_outcomes`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    outcomeType: {
      sql: `outcome_type`,
      type: `string`
    },
    
    measure: {
      sql: `measure`,
      type: `string`
    },
    
    timeFrame: {
      sql: `time_frame`,
      type: `string`
    },
    
    population: {
      sql: `population`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
