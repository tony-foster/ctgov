cube(`Documents`, {
  sql: `SELECT * FROM ctgov.documents`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, documentId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    documentId: {
      sql: `document_id`,
      type: `string`
    },
    
    documentType: {
      sql: `document_type`,
      type: `string`
    },
    
    url: {
      sql: `url`,
      type: `string`
    },
    
    comment: {
      sql: `comment`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
