cube(`Facilities`, {
  sql: `SELECT * FROM ctgov.facilities`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, city, country, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    status: {
      sql: `status`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    city: {
      sql: `city`,
      type: `string`
    },
    
    state: {
      sql: `state`,
      type: `string`
    },
    
    zip: {
      sql: `zip`,
      type: `string`
    },
    
    country: {
      sql: `country`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
