cube(`OutcomeAnalysisGroups`, {
  sql: `SELECT * FROM ctgov.outcome_analysis_groups`,
  
  joins: {
    OutcomeAnalyses: {
      sql: `${CUBE}.outcome_analysis_id = ${OutcomeAnalyses}.id`,
      relationship: `belongsTo`
    },
    
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
