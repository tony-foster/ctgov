cube(`BaselineCounts`, {
  sql: `SELECT * FROM ctgov.baseline_counts`,
  
  joins: {
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      sql: `count`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    units: {
      sql: `units`,
      type: `string`
    },
    
    scope: {
      sql: `scope`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
