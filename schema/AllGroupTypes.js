cube(`AllGroupTypes`, {
  sql: `SELECT * FROM ctgov.all_group_types`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, names]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    names: {
      sql: `names`,
      type: `string`
    }
  }
});
