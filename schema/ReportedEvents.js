cube(`ReportedEvents`, {
  sql: `SELECT * FROM ctgov.reported_events`,
  
  joins: {
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    },
    
    eventCount: {
      sql: `event_count`,
      type: `sum`
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    timeFrame: {
      sql: `time_frame`,
      type: `string`
    },
    
    eventType: {
      sql: `event_type`,
      type: `string`
    },
    
    defaultVocab: {
      sql: `default_vocab`,
      type: `string`
    },
    
    defaultAssessment: {
      sql: `default_assessment`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    organSystem: {
      sql: `organ_system`,
      type: `string`
    },
    
    adverseEventTerm: {
      sql: `adverse_event_term`,
      type: `string`
    },
    
    vocab: {
      sql: `vocab`,
      type: `string`
    },
    
    assessment: {
      sql: `assessment`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
