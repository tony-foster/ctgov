cube(`IdInformation`, {
  sql: `SELECT * FROM ctgov.id_information`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, idType, idValue, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    idType: {
      sql: `id_type`,
      type: `string`
    },
    
    idValue: {
      sql: `id_value`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
