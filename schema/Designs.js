cube(`Designs`, {
  sql: `SELECT * FROM ctgov.designs`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    allocation: {
      sql: `allocation`,
      type: `string`
    },
    
    interventionModel: {
      sql: `intervention_model`,
      type: `string`
    },
    
    observationalModel: {
      sql: `observational_model`,
      type: `string`
    },
    
    primaryPurpose: {
      sql: `primary_purpose`,
      type: `string`
    },
    
    timePerspective: {
      sql: `time_perspective`,
      type: `string`
    },
    
    masking: {
      sql: `masking`,
      type: `string`
    },
    
    maskingDescription: {
      sql: `masking_description`,
      type: `string`
    },
    
    interventionModelDescription: {
      sql: `intervention_model_description`,
      type: `string`
    },
    
    subjectMasked: {
      sql: `subject_masked`,
      type: `string`
    },
    
    caregiverMasked: {
      sql: `caregiver_masked`,
      type: `string`
    },
    
    investigatorMasked: {
      sql: `investigator_masked`,
      type: `string`
    },
    
    outcomesAssessorMasked: {
      sql: `outcomes_assessor_masked`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
