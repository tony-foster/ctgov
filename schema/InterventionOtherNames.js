cube(`InterventionOtherNames`, {
  sql: `SELECT * FROM ctgov.intervention_other_names`,
  
  joins: {
    Interventions: {
      sql: `${CUBE}.intervention_id = ${Interventions}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
