cube(`Eligibilities`, {
  sql: `SELECT * FROM ctgov.eligibilities`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    samplingMethod: {
      sql: `sampling_method`,
      type: `string`
    },
    
    gender: {
      sql: `gender`,
      type: `string`
    },
    
    minimumAge: {
      sql: `minimum_age`,
      type: `string`
    },
    
    maximumAge: {
      sql: `maximum_age`,
      type: `string`
    },
    
    healthyVolunteers: {
      sql: `healthy_volunteers`,
      type: `string`
    },
    
    population: {
      sql: `population`,
      type: `string`
    },
    
    criteria: {
      sql: `criteria`,
      type: `string`
    },
    
    genderDescription: {
      sql: `gender_description`,
      type: `string`
    },
    
    genderBased: {
      sql: `gender_based`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
