cube(`DesignGroupInterventions`, {
  sql: `SELECT * FROM ctgov.design_group_interventions`,
  
  joins: {
    DesignGroups: {
      sql: `${CUBE}.design_group_id = ${DesignGroups}.id`,
      relationship: `belongsTo`
    },
    
    Interventions: {
      sql: `${CUBE}.intervention_id = ${Interventions}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
