cube(`BrowseInterventions`, {
  sql: `SELECT * FROM ctgov.browse_interventions`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    meshTerm: {
      sql: `mesh_term`,
      type: `string`
    },
    
    downcaseMeshTerm: {
      sql: `downcase_mesh_term`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
