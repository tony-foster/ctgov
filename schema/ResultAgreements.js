cube(`ResultAgreements`, {
  sql: `SELECT * FROM ctgov.result_agreements`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    piEmployee: {
      sql: `pi_employee`,
      type: `string`
    },
    
    agreement: {
      sql: `agreement`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
