cube(`FacilityInvestigators`, {
  sql: `SELECT * FROM ctgov.facility_investigators`,
  
  joins: {
    Facilities: {
      sql: `${CUBE}.facility_id = ${Facilities}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    role: {
      sql: `role`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
