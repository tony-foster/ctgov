cube(`Sponsors`, {
  sql: `SELECT * FROM ctgov.sponsors`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    agencyClass: {
      sql: `agency_class`,
      type: `string`
    },
    
    leadOrCollaborator: {
      sql: `lead_or_collaborator`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
