cube(`PendingResults`, {
  sql: `SELECT * FROM ctgov.pending_results`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, eventDateDescription, id, eventDate]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    event: {
      sql: `event`,
      type: `string`
    },
    
    eventDateDescription: {
      sql: `event_date_description`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    eventDate: {
      sql: `event_date`,
      type: `time`
    }
  }
});
