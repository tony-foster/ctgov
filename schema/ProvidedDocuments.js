cube(`ProvidedDocuments`, {
  sql: `SELECT * FROM ctgov.provided_documents`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id, documentDate]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    documentType: {
      sql: `document_type`,
      type: `string`
    },
    
    hasProtocol: {
      sql: `has_protocol`,
      type: `string`
    },
    
    hasIcf: {
      sql: `has_icf`,
      type: `string`
    },
    
    hasSap: {
      sql: `has_sap`,
      type: `string`
    },
    
    url: {
      sql: `url`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    documentDate: {
      sql: `document_date`,
      type: `time`
    }
  }
});
