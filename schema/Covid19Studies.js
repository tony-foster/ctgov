cube(`Covid19Studies`, {
  sql: `SELECT * FROM ctgov.covid_19_studies`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, officialTitle, otherIds, lastUpdatePostedDate, studyFirstSubmittedDate, resultsFirstPostedDate, startDate, primaryCompletionDate, completionDate, studyFirstPostedDate, nlmDownloadDate]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    overallStatus: {
      sql: `overall_status`,
      type: `string`
    },
    
    studyType: {
      sql: `study_type`,
      type: `string`
    },
    
    officialTitle: {
      sql: `official_title`,
      type: `string`
    },
    
    acronym: {
      sql: `acronym`,
      type: `string`
    },
    
    phase: {
      sql: `phase`,
      type: `string`
    },
    
    whyStopped: {
      sql: `why_stopped`,
      type: `string`
    },
    
    hasDmc: {
      sql: `has_dmc`,
      type: `string`
    },
    
    isFdaRegulatedDevice: {
      sql: `is_fda_regulated_device`,
      type: `string`
    },
    
    isFdaRegulatedDrug: {
      sql: `is_fda_regulated_drug`,
      type: `string`
    },
    
    isUnapprovedDevice: {
      sql: `is_unapproved_device`,
      type: `string`
    },
    
    hasExpandedAccess: {
      sql: `has_expanded_access`,
      type: `string`
    },
    
    hasSingleFacility: {
      sql: `has_single_facility`,
      type: `string`
    },
    
    leadSponsor: {
      sql: `lead_sponsor`,
      type: `string`
    },
    
    otherIds: {
      sql: `other_ids`,
      type: `string`
    },
    
    gender: {
      sql: `gender`,
      type: `string`
    },
    
    genderBased: {
      sql: `gender_based`,
      type: `string`
    },
    
    genderDescription: {
      sql: `gender_description`,
      type: `string`
    },
    
    population: {
      sql: `population`,
      type: `string`
    },
    
    minimumAge: {
      sql: `minimum_age`,
      type: `string`
    },
    
    maximumAge: {
      sql: `maximum_age`,
      type: `string`
    },
    
    criteria: {
      sql: `criteria`,
      type: `string`
    },
    
    healthyVolunteers: {
      sql: `healthy_volunteers`,
      type: `string`
    },
    
    keywords: {
      sql: `keywords`,
      type: `string`
    },
    
    interventions: {
      sql: `interventions`,
      type: `string`
    },
    
    conditions: {
      sql: `conditions`,
      type: `string`
    },
    
    primaryPurpose: {
      sql: `primary_purpose`,
      type: `string`
    },
    
    allocation: {
      sql: `allocation`,
      type: `string`
    },
    
    observationalModel: {
      sql: `observational_model`,
      type: `string`
    },
    
    interventionModel: {
      sql: `intervention_model`,
      type: `string`
    },
    
    masking: {
      sql: `masking`,
      type: `string`
    },
    
    subjectMasked: {
      sql: `subject_masked`,
      type: `string`
    },
    
    caregiverMasked: {
      sql: `caregiver_masked`,
      type: `string`
    },
    
    investigatorMasked: {
      sql: `investigator_masked`,
      type: `string`
    },
    
    outcomesAssessorMasked: {
      sql: `outcomes_assessor_masked`,
      type: `string`
    },
    
    designOutcomes: {
      sql: `design_outcomes`,
      type: `string`
    },
    
    briefSummary: {
      sql: `brief_summary`,
      type: `string`
    },
    
    detailedDescription: {
      sql: `detailed_description`,
      type: `string`
    },
    
    lastUpdatePostedDate: {
      sql: `last_update_posted_date`,
      type: `time`
    },
    
    studyFirstSubmittedDate: {
      sql: `study_first_submitted_date`,
      type: `time`
    },
    
    resultsFirstPostedDate: {
      sql: `results_first_posted_date`,
      type: `time`
    },
    
    startDate: {
      sql: `start_date`,
      type: `time`
    },
    
    primaryCompletionDate: {
      sql: `primary_completion_date`,
      type: `time`
    },
    
    completionDate: {
      sql: `completion_date`,
      type: `time`
    },
    
    studyFirstPostedDate: {
      sql: `study_first_posted_date`,
      type: `time`
    },
    
    nlmDownloadDate: {
      sql: `nlm_download_date`,
      type: `time`
    }
  }
});
