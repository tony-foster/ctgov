cube(`ResultContacts`, {
  sql: `SELECT * FROM ctgov.result_contacts`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    organization: {
      sql: `organization`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    phone: {
      sql: `phone`,
      type: `string`
    },
    
    email: {
      sql: `email`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
