cube(`BaselineMeasurements`, {
  sql: `SELECT * FROM ctgov.baseline_measurements`,
  
  joins: {
    ResultGroups: {
      sql: `${CUBE}.result_group_id = ${ResultGroups}.id`,
      relationship: `belongsTo`
    }
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, title, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    ctgovGroupCode: {
      sql: `ctgov_group_code`,
      type: `string`
    },
    
    classification: {
      sql: `classification`,
      type: `string`
    },
    
    category: {
      sql: `category`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    units: {
      sql: `units`,
      type: `string`
    },
    
    paramType: {
      sql: `param_type`,
      type: `string`
    },
    
    paramValue: {
      sql: `param_value`,
      type: `string`
    },
    
    dispersionType: {
      sql: `dispersion_type`,
      type: `string`
    },
    
    dispersionValue: {
      sql: `dispersion_value`,
      type: `string`
    },
    
    explanationOfNa: {
      sql: `explanation_of_na`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
