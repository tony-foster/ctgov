cube(`ResponsibleParties`, {
  sql: `SELECT * FROM ctgov.responsible_parties`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, title, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    responsiblePartyType: {
      sql: `responsible_party_type`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    organization: {
      sql: `organization`,
      type: `string`
    },
    
    affiliation: {
      sql: `affiliation`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
