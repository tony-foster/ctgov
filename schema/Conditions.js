cube(`Conditions`, {
  sql: `SELECT * FROM ctgov.conditions`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, name, downcaseName, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    name: {
      sql: `name`,
      type: `string`
    },
    
    downcaseName: {
      sql: `downcase_name`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
