cube(`ParticipantFlows`, {
  sql: `SELECT * FROM ctgov.participant_flows`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, id]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    recruitmentDetails: {
      sql: `recruitment_details`,
      type: `string`
    },
    
    preAssignmentDetails: {
      sql: `pre_assignment_details`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    }
  }
});
