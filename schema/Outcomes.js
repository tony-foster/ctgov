cube(`Outcomes`, {
  sql: `SELECT * FROM ctgov.outcomes`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, title, id, anticipatedPostingDate]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    outcomeType: {
      sql: `outcome_type`,
      type: `string`
    },
    
    title: {
      sql: `title`,
      type: `string`
    },
    
    description: {
      sql: `description`,
      type: `string`
    },
    
    timeFrame: {
      sql: `time_frame`,
      type: `string`
    },
    
    population: {
      sql: `population`,
      type: `string`
    },
    
    anticipatedPostingMonthYear: {
      sql: `anticipated_posting_month_year`,
      type: `string`
    },
    
    units: {
      sql: `units`,
      type: `string`
    },
    
    unitsAnalyzed: {
      sql: `units_analyzed`,
      type: `string`
    },
    
    dispersionType: {
      sql: `dispersion_type`,
      type: `string`
    },
    
    paramType: {
      sql: `param_type`,
      type: `string`
    },
    
    id: {
      sql: `id`,
      type: `number`,
      primaryKey: true
    },
    
    anticipatedPostingDate: {
      sql: `anticipated_posting_date`,
      type: `time`
    }
  }
});
