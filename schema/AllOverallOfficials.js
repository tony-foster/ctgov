cube(`AllOverallOfficials`, {
  sql: `SELECT * FROM ctgov.all_overall_officials`,
  
  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [nctId, names]
    }
  },
  
  dimensions: {
    nctId: {
      sql: `nct_id`,
      type: `string`
    },
    
    names: {
      sql: `names`,
      type: `string`
    }
  }
});
